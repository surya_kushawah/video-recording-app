package com.laeiou.mobile.thirdi;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import com.laeiou.mobile.thirdi.utils.CameraConstants;

public class MainActivity extends AppCompatActivity
    implements View.OnClickListener, CameraFragment.Callback {

  private CameraFragment cameraFragment;

  /**
   * The button of record video
   */
  private ImageButton record;

  /**
   * The button of switch camera
   */
  private ImageButton switchCamera;

  /**
   * The button of view video
   */
  private ImageButton viewVideo;

  private String videoUri;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    setFullScreen();
    if (null == savedInstanceState) {
      cameraFragment = CameraFragment.newInstance();
      getFragmentManager().beginTransaction()
          .replace(R.id.container, cameraFragment)
          .commit();
    } else {
      cameraFragment = (CameraFragment) getFragmentManager().findFragmentById(R.id.container);
    }
    cameraFragment.setCallback(this);
    record = findViewById(R.id.camera_record_button);
    viewVideo = findViewById(R.id.video_view_button);
    viewVideo.setOnClickListener(this);
    record.setOnClickListener(this);
    switchCamera = findViewById(R.id.camera_switch_button);
    switchCamera.setOnClickListener(this);

    int height = getResources().getDisplayMetrics().heightPixels / 4;
    ConstraintLayout controlLayout = findViewById(R.id.control);
    RelativeLayout.LayoutParams layoutParams =
        (RelativeLayout.LayoutParams) controlLayout.getLayoutParams();
    layoutParams.height = height;
    controlLayout.setLayoutParams(layoutParams);
  }

  private void setFullScreen() {
    getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION |
        WindowManager.LayoutParams.FLAG_FULLSCREEN);
  }

  @Override public void onClick(View view) {
    switch (view.getId()) {
      case R.id.camera_record_button:
        if (cameraFragment.isRecordingVideo()) {
          cameraFragment.stopRecordingVideo();
          record.setImageResource(R.drawable.ic_start);
          switchCamera.setVisibility(View.VISIBLE);
          viewVideo.setVisibility(View.VISIBLE);
        } else {
          cameraFragment.startRecordingVideo();
          record.setImageResource(R.drawable.ic_stop);
          viewVideo.setVisibility(View.GONE);
          switchCamera.setVisibility(View.GONE);
          Toast.makeText(this, getString(R.string.message_recording_start), Toast.LENGTH_SHORT)
              .show();
        }

        invalidateOptionsMenu();
        break;
      case R.id.camera_switch_button:
        int facing = cameraFragment.getFacing();
        cameraFragment.setFacing(facing == CameraConstants.FACING_FRONT ?
            CameraConstants.FACING_BACK : CameraConstants.FACING_FRONT);

        invalidateOptionsMenu();
        break;
      case R.id.video_view_button:
        if (videoUri != null) {
          Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(videoUri));
          intent.setDataAndType(Uri.parse(videoUri), "video/*");
          startActivity(intent);
        }
        break;
    }
  }

  @Override public void onVideoRecorded(String videoUri) {
    this.videoUri = videoUri;
  }
}
