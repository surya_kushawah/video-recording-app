package com.laeiou.mobile.thirdi;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import androidx.fragment.app.FragmentActivity;
import com.amazonaws.mobile.client.AWSMobileClient;
import com.amazonaws.mobile.client.Callback;
import com.amazonaws.mobile.client.SignInUIOptions;
import com.amazonaws.mobile.client.UserStateDetails;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class RecordingActivity extends FragmentActivity implements OnMapReadyCallback {
  private GoogleMap mMap;
  private static final String TAG = "RecordingActivity";
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_recording);
    SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
        .findFragmentById(R.id.map);
    mapFragment.getMapAsync(this);

    final AWSMobileClient auth = AWSMobileClient.getInstance();

    if (auth.isSignedIn()) {
      //ActivityUtils.startActivity(this, SimpleNavActivity.class);
    } else {
      auth.showSignIn(this,
          SignInUIOptions.builder()
              .nextActivity(MainActivity.class)
              .build(),
          new Callback<UserStateDetails>() {
            @Override
            public void onResult(UserStateDetails result) {
              Log.d(TAG, "onResult: User signed-in " + result.getUserState());
            }

            @Override
            public void onError(final Exception e) {
              runOnUiThread(new Runnable() {
                @Override
                public void run() {
                  Log.e(TAG, "onError: User sign-in error", e);
                  Toast.makeText(RecordingActivity.this, "User sign-in error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
              });
            }
          });
    }
  }

  @Override
  public void onMapReady(GoogleMap googleMap) {
    mMap = googleMap;
    // Add a marker in Sydney, Australia, and move the camera.
    LatLng sydney = new LatLng(-34, 151);
    mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
    mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
  }
}
